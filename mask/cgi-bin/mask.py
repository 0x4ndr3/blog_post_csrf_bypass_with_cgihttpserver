#!/usr/bin/env python
import cgi,cgitb
from mechanize import Browser
cgitb.enable() # enables appropriate output for browsers in case of errors

URL = 'http://192.168.0.184/csrf-token/index.php'

def respond(string):
    print "Content-Type: text/html"
    print
    print string
    quit()

form = cgi.FieldStorage()
u = form["username"].value
p = form["password"].value

b = Browser()
b.set_handle_robots(False)
b.open(URL)
b.select_form(nr=0)
b.form['username'] = u
b.form['password'] = p
b.submit()
respond(b.title())
